<?php

namespace App\Controller\Admin;

use App\Entity\Concert;
use App\Entity\Artiste;
use App\Entity\Email;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Administration de la Sirène');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToRoute('Retour au site', 'fas fa-sign-out-alt', 'homepage');
        yield MenuItem::section('Admin', 'fas fa-cogs');
        yield MenuItem::linkToCrud('Les concerts', 'fas fa-volume-up', Concert::class)
            ->setController(ConcertCrudController::class);
        yield MenuItem::linkToCrud('Les concerts passés', 'fas fa-volume-down', Concert::class)
            ->setController(ConcertPassedCrudController::class);
        yield MenuItem::linkToCrud('Les artistes', 'fas fa-user-cog', Artiste::class);
        yield MenuItem::linkToCrud('Newsletter', 'fas fa-paper-plane', Email::class);
    }
}
